import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'invoices',
    loadChildren: () => import('./home/invoices/invoices.module').then( m => m.InvoicesPageModule)
  },
  {
    path: 'licenses',
    loadChildren: () => import('./home/licenses/licenses.module').then( m => m.LicensesPageModule)
  },
  {
    path: 'reminders',
    loadChildren: () => import('./home/reminders/reminders.module').then( m => m.RemindersPageModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
