import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Plugins } from '@capacitor/core';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ReminderData } from '../home/reminders/reminders.service';

export class AuthResponseData {
  email: string;
  session: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
private _user = new BehaviorSubject<User>(null);
private activeLogoutTimer: any;

  constructor(private http: HttpClient, private router: Router) { }

  get userId() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.email;
        } else {
          return null;
        }
      })
    );
  }

  get userIsAuthenticated() {
    return this._user.asObservable().pipe(
      map(user => {
        if(user) {
          return true;
          //return true;
        } else {
          return false;
          //return true;
        }
      })
    )
  }

  userEqualsTo(resData: {[key: string]: ReminderData}, key: string) {
    return this._user.asObservable().pipe(
      map(user => {
        if (user.email === resData[key].userId) {
          return true;
        } else {
          return false;
        }
      })
    );
  }

  login(email: string, password: string) {

    const stringToEncode = email + ':' + password;
    const base64encodedAuth = btoa(stringToEncode);
    console.log(base64encodedAuth);

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic ' + base64encodedAuth
      }),
      observe: 'response' as 'body',
      withCredentials: true
    };
    return this.http.get(
      'http://212.227.10.211:8080/pp-app-server/authentication', httpOptions
    );

  }

  logout() {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer);
    }
    this._user.next(null);
    Plugins.Storage.remove({key: 'authData'});
    this.router.navigateByUrl('/auth');
  }

  setUserData(userData: AuthResponseData) {
    const user = new User(
      userData.email
    );
    this._user.next(user);
  }
}
