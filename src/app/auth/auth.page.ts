import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthResponseData, AuthService } from './auth.service';
import { Router } from '@angular/router';
import { HttpEvent, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage {
  isLoading = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;

    this.authenticate(email, password);
    form.reset();
  }

  authenticate(email: string, password: string) {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Einloggen...'})
      .then(loadingEl => {
        loadingEl.present();
        this.authService.login(email, password).subscribe((response: HttpEvent<any>) => {
          if(response.type === HttpEventType.Response) {
            const cookie = response.headers.get('Set-Cookie');
            const userAuth = new AuthResponseData();
            userAuth.email = email;
            userAuth.session = cookie;
            this.authService.setUserData(userAuth);
            this.isLoading = false;
            loadingEl.dismiss();
            this.router.navigateByUrl('/home');
          }
        }, error => {
          loadingEl.dismiss();
          this.alertCtrl.create({
            header: 'Authentifikation fehlgeschlagen',
            message: 'Bitte überprüfen Sie ihre Daten und versuchen Sie es erneut',
            buttons: ['Okay'],
          }).then(alertEl => alertEl.present());
        });
      });
  }
}
