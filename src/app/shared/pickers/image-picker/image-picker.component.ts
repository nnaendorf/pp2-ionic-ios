import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { Plugins, Capacitor, CameraSource, CameraResultType } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss'],
})
export class ImagePickerComponent implements OnInit {
  @ViewChild('filePicker', {static: false}) filePickerRef: ElementRef<HTMLInputElement>;
  @Output() imagePick = new EventEmitter<string | File>();
  @Input() showPreview = false;
  selectedImage: string;
  usePicker = false;
  isInvoice = false;

  constructor(private platform: Platform, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.route.routeConfig.component.name === 'InvoicesPage') {
      this.isInvoice = true;
    } else {
      this.isInvoice = false;
    }
  }

  onPickImage() {
    console.log(this.usePicker);
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.filePickerRef.nativeElement.click();
      console.log("Kamera nicht verfügbar");
      return;
    }
    if (this.usePicker) {
      this.filePickerRef.nativeElement.click();
      console.log("Kamera immernoch nicht verfügbar");
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 50,
      source: CameraSource.Prompt,
      correctOrientation: true,
      width: 600,
      resultType: CameraResultType.Base64
    }).then(image => {
      this.selectedImage = 'data:image/jpg;base64,' + image.base64String;
      this.imagePick.emit(image.base64String);
    }).catch(error => {
      console.log(error);
      return false;
    });
  }

  onFileChosen(event: Event) {
    const pickedFile = (event.target as HTMLInputElement).files[0];
    if (!pickedFile) {
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      const dataUrl = fileReader.result.toString();
      this.selectedImage = dataUrl;
      this.imagePick.emit(pickedFile);
    };
    fileReader.readAsDataURL(pickedFile);
  }

  usePickerTrue() {
    this.usePicker = true;
  }

  usePickerFalse() {
    this.usePicker = false;
  }
}
