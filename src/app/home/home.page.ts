import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { RemindersService } from './reminders/reminders.service';
import { Reminder } from './reminders/reminder.model';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Platform, AlertController } from '@ionic/angular';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  remindersSub: Subscription;
  loadedReminders: Reminder[];
  currentDate = new Date();
  isLoading = false;
  allowPersonal: boolean;
  token: string;

  constructor(
    private authService: AuthService,
    private remindersService: RemindersService,
    private readonly http: HttpClient,
    platform: Platform,
    private alertCtrl: AlertController,
  ) {
    platform.ready().then(() => {
      window['FirebasePlugin'].getToken(token => this.token = token,
                                        error => console.error('Error getting token', error));

      window['FirebasePlugin'].onTokenRefresh(token => this.token = token,
                                        error => console.error('Error token refresh', error));

      window['FirebasePlugin'].onMessageReceived(notification => this.handleNotification(notification),
                                        error => console.error('Error notification open', error));
    });

    const personalFlag = localStorage.getItem('allowPersonal');
    this.allowPersonal = personalFlag != null ? JSON.parse(personalFlag) : false;

  }

  register() {
    const formData = new FormData();
    formData.append('token', this.token);
    this.http.post('http://212.227.10.211:8080/pp-app-server/register', formData, {withCredentials: true})
      .pipe(timeout(10000))
      .subscribe(() => localStorage.setItem('allowPersonal', JSON.stringify(this.allowPersonal)),
        error => {
          alert(JSON.stringify(error));
          this.allowPersonal = !this.allowPersonal;
      });
  }

  unregister() {
    const formData = new FormData();
    formData.append('token', this.token);
    this.http.post('http://212.227.10.211:8080/pp-app-server/unregister', formData, {withCredentials: true})
      .pipe(timeout(10000))
      .subscribe(() => localStorage.setItem('allowPersonal', JSON.stringify(this.allowPersonal)),
        error => {
          alert(JSON.stringify(error));
          this.allowPersonal = !this.allowPersonal;
      });
  }

  onPmChange() {
    if (this.allowPersonal) {
      this.register();
    } else {
      this.unregister();
    }
  }

  handleNotification(data) {

    this.alertCtrl.create({
      header: data.title,
      message: data.body,
      buttons: ['OK'],
    }).then(alertEl => {
      alertEl.present();
    });

  }

  ngOnInit() {
    this.remindersSub = this.remindersService.reminders.subscribe(reminders => {
      this.loadedReminders = reminders.filter((reminder) => {
        return reminder.startTime >= new Date();
      });
    });
  }

  ionViewDidEnter() {
    this.isLoading = true;
    this.remindersService.fetchReminders().subscribe(() => {
      this.isLoading = false;
    });
  }

  ngOnDestroy() {
    this.remindersSub.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }
}

