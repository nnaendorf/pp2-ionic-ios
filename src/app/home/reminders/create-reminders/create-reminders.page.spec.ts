import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateRemindersPage } from './create-reminders.page';

describe('CreateRemindersPage', () => {
  let component: CreateRemindersPage;
  let fixture: ComponentFixture<CreateRemindersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRemindersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateRemindersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
