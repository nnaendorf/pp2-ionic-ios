import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateRemindersPageRoutingModule } from './create-reminders-routing.module';

import { CreateRemindersPage } from './create-reminders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateRemindersPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateRemindersPage]
})
export class CreateRemindersPageModule {}
