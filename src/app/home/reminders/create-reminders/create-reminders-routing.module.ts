import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateRemindersPage } from './create-reminders.page';

const routes: Routes = [
  {
    path: '',
    component: CreateRemindersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateRemindersPageRoutingModule {}
