import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RemindersService } from '../reminders.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-create-reminders',
  templateUrl: './create-reminders.page.html',
  styleUrls: ['./create-reminders.page.scss'],
})
export class CreateRemindersPage implements OnInit {
  form: FormGroup;
  isAllDay = false;

  constructor(private remindersService: RemindersService, private router: Router, private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]}),
      description: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]}),
      startTime: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]}),
      endTime: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]}),
      allDay: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]})
    });
  }

  onSubmitReminding() {
    if (!this.form.valid) {
      return;
    }
    this.loadingCtrl.create({
      message: 'Erinnerung wird erstellt...'
    })
    .then(loadingEl => {
      loadingEl.present();
      return this.remindersService.addReminder(
        this.form.value.title,
        this.form.value.description,
        this.form.value.startTime,
        this.form.value.endTime,
        this.form.value.allDay,
      ).subscribe(() => {
        loadingEl.dismiss();
        this.form.reset();
        this.router.navigateByUrl('/reminders');
      });
    });
  }

  allDayChecked() {
    this.isAllDay = !this.isAllDay;
    if (this.isAllDay) {
      this.form.get('endTime').setValue(this.form.value.startTime);
    }
  }
}
