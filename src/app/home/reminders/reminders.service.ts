import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Reminder } from './reminder.model';
import { AuthService } from 'src/app/auth/auth.service';
import { take, switchMap, tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

export interface ReminderData {
  userId: string;
  title: string;
  description: string;
  startTime: string;
  endTime: string;
  allDay: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class RemindersService {
  private _reminders = new BehaviorSubject<Reminder[]>([]);
  private userId: string;

  constructor(private authService: AuthService, private http: HttpClient) { }

  get reminders() {
    return this._reminders.asObservable();
  }

  fetchReminders() {
    this.authService.userId.subscribe(userId => this.userId = userId);
    return this.http
      .get<{[key: string]: ReminderData}>('http://212.227.10.211:8080/pp-app-server/reminder'
      , {withCredentials: true})
      .pipe(map(resData => {
        const reminders = [];
        for (const key in resData) {
          if (resData.hasOwnProperty(key)) {
            if (!resData[key].allDay) {
              reminders.push(
                new Reminder(
                  key,
                  resData[key].userId,
                  resData[key].title,
                  resData[key].description,
                  new Date(resData[key].startTime),
                  new Date(resData[key].endTime),
                  resData[key].allDay,
                )
              );
            } else if (resData[key].allDay) {
              reminders.push(
                new Reminder(
                  key,
                  resData[key].userId,
                  resData[key].title,
                  resData[key].description,
                  new Date(
                    Date.UTC(
                      new Date(resData[key].startTime).getUTCFullYear(),
                      new Date(resData[key].startTime).getUTCMonth(),
                      new Date(resData[key].startTime).getUTCDate())),
                  new Date(
                    Date.UTC(
                      new Date(resData[key].endTime).getUTCFullYear(),
                      new Date(resData[key].endTime).getUTCMonth(),
                      new Date(resData[key].endTime).getUTCDate() + 1)),
                  resData[key].allDay,
                )
              );
            }
          }
        }
        reminders.sort((a, b) => a.startTime - b.startTime);
        return reminders;
      }),
      tap(reminders => {
        this._reminders.next(reminders);
      })
    );
  }

  addReminder(title: string, description: string, startTime: Date, endTime: Date, allDay: boolean) {
    const generatedId = Math.random().toString();
    let newReminder: Reminder;
    return this.authService.userId.pipe(
      take(1),
      switchMap(userId => {
      if (!userId) {
        throw new Error('Could not find user');
      }
      newReminder = new Reminder(
        generatedId,
        userId,
        title,
        description,
        startTime,
        endTime,
        allDay,
      );
      const uploadData = new FormData();
      uploadData.append('id', generatedId);
      uploadData.append('userId', userId);
      uploadData.append('title', title);
      uploadData.append('description', description);
      uploadData.append('startTime', startTime.toString());
      uploadData.append('endTime', endTime.toString());
      uploadData.append('allDay', allDay.toString());
      return this.http.post<{name: string}>('http://212.227.10.211:8080/pp-app-server/reminder',
      uploadData, {withCredentials: true});
    }), switchMap(resData => {
        return this.reminders;
      }),
      take(1),
      tap(reminders => {
        this._reminders.next(reminders.concat(newReminder));
      })
    );
  }
}
