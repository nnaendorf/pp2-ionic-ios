import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  constructor(private http: HttpClient) { }

  uploadImage(invoice: File, title: string, description: string, price: number) {
    const uploadData = new FormData();
    uploadData.append('invoice', invoice);
    uploadData.append('title', title);
    uploadData.append('description', description),
    uploadData.append('price', price.toString());

    return this.http.post<{status: string}>(
      'http://212.227.10.211:8080/pp-app-server/invoices', uploadData, {withCredentials: true}
    );
  }
}
