import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { InvoicesService } from './invoices.service';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Invoice } from './invoice.model';

function base64toBlob(base64Data, contentType) {
  contentType = contentType || '';
  const sliceSize = 1024;
  const byteCharacters = window.atob(base64Data);
  const bytesLength = byteCharacters.length;
  const slicesCount = Math.ceil(bytesLength / sliceSize);
  const byteArrays = new Array(slicesCount);

  for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    const begin = sliceIndex * sliceSize;
    const end = Math.min(begin + sliceSize, bytesLength);

    const bytes = new Array(end - begin);
    for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, {type: contentType});
}

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {
  form: FormGroup;
  invoice: Invoice;

  constructor(private invoicesService: InvoicesService, private loadingCtrl: LoadingController, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {updateOn: 'blur', validators: [Validators.required]}),
      description: new FormControl(null, {updateOn: 'blur', validators: [Validators.required, Validators.maxLength(200)]}),
      price: new FormControl(null, {updateOn: 'blur', validators: [Validators.required, Validators.min(1)]}),
      image: new FormControl(null)
    });
  }

  onImagePicked(imageData: string | File) {
    let imageFile;
    if (typeof imageData === 'string') {
      try {
        imageFile = base64toBlob(imageData, 'image/jpeg');
      } catch (error) {
        console.log(error);
        return;
      }
    } else {
      imageFile = imageData;
    }
    this.form.patchValue({image: imageFile});
  }

  onSubmitInvoice() {
    if (!this.form.valid || !this.form.get('image').value) {
      return;
    }
    this.invoice = new Invoice(
      this.form.value.title,
      this.form.value.description,
      this.form.value.price,
      this.form.get('image').value,
    );
    this.loadingCtrl.create({
      message: 'Beleg wird eingereicht...'
    })
    .then(loadingEl => {
      loadingEl.present();
      this.invoicesService.uploadImage(
        this.invoice.image,
        this.invoice.title,
        this.invoice.description,
        this.invoice.price
      ).subscribe(() => {
        loadingEl.dismiss();
        this.form.reset();
        this.router.navigate(['/home']);
      });
    });

  }

}
