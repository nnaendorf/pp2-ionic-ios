export class Invoice {
    constructor(
        public title: string,
        public description: string,
        public price: number,
        public image: File,
    ) {}
}
