import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LicensesService {

  constructor(private http: HttpClient) {}

  uploadImages(frontside: File, backside: File) {
    const uploadData = new FormData();
    uploadData.append('frontside', frontside);
    uploadData.append('backside', backside);

    return this.http.post<{status: string}>(
      'http://212.227.10.211:8080/pp-app-server/driverlicense', uploadData, {withCredentials: true}
    );
  }
}
