import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LicensesPageRoutingModule } from './licenses-routing.module';

import { LicensesPage } from './licenses.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LicensesPageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [LicensesPage]
})
export class LicensesPageModule {}
